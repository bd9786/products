#include "pch.h"
#include "NonPerishableProduct.h"
#include "NonPerishableProductType.h"


NonPerishableProduct::NonPerishableProduct(uint32_t id, std::string name, float rawPrice, NonPerishableProductType type) :
	m_id(id), m_name(name), m_rawPrice(rawPrice), m_type(type)
{
	// empty
}

NonPerishableProductType NonPerishableProduct::GetType() { return m_type; };
uint32_t NonPerishableProduct::GetVAT() { return 19; };
float NonPerishableProduct::GetPrice() { return (m_rawPrice + GetVAT() / 100 * m_rawPrice); };