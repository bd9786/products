#include "pch.h"
#include "Product.h"

 Product::Product(uint32_t id, const std::string & name, float price) : 
	m_id(id), m_name(name), m_rawPrice(price)
{
	// empty
}
 Product::Product() :
	 m_id(NULL), m_name(NULL), m_rawPrice(NULL)
 {
	 // empty
 }

uint32_t Product:: GetID() { return m_id; }
std::string Product::GetName() { return m_name; }
float Product::GetRawPrice() { return m_rawPrice; }