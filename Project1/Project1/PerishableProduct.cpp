#include "pch.h"
#include "PerishableProduct.h"


PerishableProduct::PerishableProduct(uint32_t id, std::string name, float rawPrice, std::string expirationDate) :
	m_id(id), m_name(name), m_rawPrice(rawPrice), m_ExpirationDate(expirationDate)
{
	// empty
}

std::string PerishableProduct::GetExpirationDate() { return m_name; };
uint32_t PerishableProduct::GetVAT() { return 9; };
float PerishableProduct::GetPrice() { return (m_rawPrice + GetVAT() / 100 * m_rawPrice); };