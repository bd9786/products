#pragma once
#include <string>
#include "Product.h"
#include "NonPerishableProductType.h"

class NonPerishableProduct : Product {
private:
	NonPerishableProductType m_type;
public:
	NonPerishableProduct(uint32_t id, std::string name, float rawPrice, NonPerishableProductType type);
	NonPerishableProductType GetType() {};
	uint32_t GetVAT() {};
	float GetPrice() {};
};
