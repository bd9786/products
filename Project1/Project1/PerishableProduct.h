#pragma once
#include <string>
#include "Product.h"

class PerishableProduct : Product {
private:
	std::string m_ExpirationDate;
public:
	PerishableProduct(uint32_t id, std::string name, float rawPrice, std::string expirationDate);
	std::string GetExpirationDate() {};
	uint32_t GetVAT() {};
	float GetPrice() {};
};