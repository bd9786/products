#include "pch.h"
#include "Product.h"
#include <algorithm>
#include <fstream>
#include <iostream>
#include <vector>

void sortByPrice(std::vector<Product> obj) {
	bool compare(Product const & first, Product const & second) {
		return first.GetPrice() < second.GetPrice();
	};
	std::sort(obj.begin(), obj.end(), compare);
}

void sortByName(std::vector<Product> obj) {
	bool compare(Product const & first, Product const & second) {
		return first.GetName() < second.GetName();
	};
	std::sort(obj.begin(), obj.end(), compare);
}

void printProd(std::vector<Product> obj) {
	for (int i = 0; i < obj.size(); i++) {
		if (obj[i].GetVAT() == 19) {
			std::cout << obj[i].GetID() << " " << obj[i].GetName() << " " << obj[i].GetPrice() << " "
				<< obj[i].GetVAT() << " " << obj[i].GetType() << "\n";
		}
	};
}

int main()
{
	std::vector<Product> vectorOfProducts;

	uint32_t id;
	std::string name;
	float price;
	uint32_t vat;
	std::string dateOrType;

	for (std::ifstream productFileIn("Products.proddb"); !productFileIn.eof();)
	{
		productFileIn >> id >> name >> price >> vat >> dateOrType;
		// validate type input...
		vectorOfProducts.push_back(Product(id, name, price, dateOrType));
	}

	printProd(vectorOfProducts);
	sortByName(vectorOfProducts);
	sortByPrice(vectorOfProducts);

	return 0;
}