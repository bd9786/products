#pragma once
#include <cstdint>
#include <string>
#include "IPriceable.h"

class Product : public IPriceable
{
public:
	Product();
	Product(uint32_t id, const std::string& name, float rawPrice);
	uint32_t GetID() {};
	std::string GetName() {};
	float GetRawPrice() {};

protected:
	uint32_t m_id;
	std::string m_name;
	float m_rawPrice;
};

